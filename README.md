
Setting up  the Connection String
-------------------------------------------------------------------------------------------------------------------------------------------------------

1.Open/click on the VS file "SMA.PatientTransport.sln".

2.In Solution Explorer collapse the "SMA.PatientTransport.DataAccess" and find "SMAContext.cs".

3.Open "SMAContext.cs" and below the line region fields is a line of code of empty connection string "private string _connectionStringName =".

4.Open up Server Explorer and connect to the server "PatientTransportUAT.dbo".

5.Get the Connection String and copy it

6.Go back to "SMAContext.cs and paste the connection string to empty line of code "private string _connectionStringName =".

7.Scroll down, inside  OnConfiguring type the line "optionBuilder.UseSqlServer(@"connectionstring").

8.Done! It's connected.

Adding the SignalR
-------------------------------------------------------------------------------------------------------------------------------------------------------

1.Collapse "SMA.PatientTransport.Web"

2.Find "appsetting.json" then open it

3.search if the code 
 "Azure":{
    "SignalR":{
	  "ConnectionString": ""
	          }
},

exist if not type it out and copy&paste the signalr connection string.

4.Done!

Fixing up some of the error
-------------------------------------------------------------------------------------------------------------------------------------------------------

Most of the error is made because the Nuget are outdated so to update.

1.Click on Tools in MS VS and hover to NuGet Package Manager click on Manage NuGet Packages in Solution.

2.Click on Updates and update the packages to the latest stable build.

3.We now have the latest build for the packages!

There's also an error because the packages in ClientApp is outdated.

1.Collapse "SMA.PatientTransport.Web".

2.Find the ClientApp folder and right click on it .

3.Click on Open folder in File Explorer and in the path type cmd.

4.In cmd type " code . " it will open a VS Code.

5.On VS Code type "npm install npm@latest -g" if it didn't work and keep getting error reinstall the node.js and try again.

6.Go back to MS VS and in package manager console type dotnet build.

7.Done!

Postman 
-------------------------------------------------------------------------------------------------------------------------------------------------------

1.Create a new collection and name it.

2.Click on the dropdown list and select Add a request.

3.Change GET to POST and in enter request url  type "http://localhost:22730/api/auth/login"

4.In Headers type Content-Type in KEY and application/json VALUE to avoid error 415.

5.Double check the body if is raw and in JSON.

6.Go back to MS VS and run it.

7.In the Web type the http://localhost:22730/api/auth/login and go back to postman and select Send.

8.Done!



